<?php

namespace Drupal\media_entity_download\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\path\Plugin\Field\FieldType\PathItem;

/**
 * Defines the 'media_download_path' entity field type.
 *
 * @FieldType(
 *   id = "media_download_path",
 *   label = @Translation("Download path"),
 *   description = @Translation("An entity field containing a path alias and related data for downloading media entities."),
 *   no_ui = TRUE,
 *   default_widget = "media_download_path",
 *   list_class = "\Drupal\media_entity_download\Plugin\Field\FieldType\MediaDownloadPathFieldItemList",
 *   constraints = {"PathAlias" = {}},
 * )
 */
class MediaDownloadPathItem extends PathItem {

  public static function getMediaDownloadPath(EntityInterface $entity) {
    return $entity->urlInfo()->getInternalPath() . '/download';
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    $alias_storage = \Drupal::service('path.alias_storage');
    if (!$update) {
      if ($this->alias) {
        if ($path = $alias_storage->save('/' . self::getMediaDownloadPath($this->getEntity()), $this->alias, $this->getLangcode())) {
          $this->pid = $path['pid'];
        }
      }
    }
    else {
      // Delete old alias if user erased it.
      if ($this->pid && !$this->alias) {
        $alias_storage->delete(['pid' => $this->pid]);
      }
      // Only save a non-empty alias.
      elseif ($this->alias) {
        $alias_storage->save('/' . self::getMediaDownloadPath($this->getEntity()), $this->alias, $this->getLangcode(), $this->pid);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $values['alias'] = '/download/' . str_replace(' ', '-', strtolower($random->sentences(3)));
    return $values;
  }

}
