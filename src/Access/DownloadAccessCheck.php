<?php

namespace Drupal\media_entity_download\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\media\MediaInterface;
use Symfony\Component\Routing\Route;

class DownloadAccessCheck implements AccessInterface {

  /**
   * Checks whether a media entity may be downloaded or not
   *
   * The value of the '_media_entity_download_access' key has to be the name
   * of the route parameter representing a media entity.
   *
   * @code
   * pattern: '/foo/{faz}/bar'
   * requirements:
   *   _media_entity_download_access: 'faz'
   * @endcode
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $requirement = $route->getRequirement('_media_entity_download_access');
    $route_params = $route_match->getParameters();
    if (!$route_params->has($requirement)) {
      return AccessResult::neutral();
    }

    $upcasted_param = $route_params->get($requirement);
    if (!$upcasted_param instanceof MediaInterface) {
      return AccessResult::forbidden();
    }

    /** @var MediaInterface $media */
    $media = $upcasted_param;
    $published = $media->isPublished();
    $uid = $media->getOwnerId();
    $bundle = $media->bundle();

    $access = AccessResult::forbidden();
    if ($published) {
      if ($account->hasPermission("download any published media")
        || $account->hasPermission("download published $bundle media")) {
        $access = AccessResult::allowed();
      }
    }
    elseif ($account->hasPermission("download any unpublished $bundle media")) {
      $access = AccessResult::allowed();
    }
    elseif ($account->isAuthenticated() && $account->id() == $uid
      && $account->hasPermission("download own unpublished $bundle media")) {
      $access = AccessResult::allowed()->cachePerUser();
    }
    return $access->cachePerPermissions()->addCacheableDependency($media);
  }
}

