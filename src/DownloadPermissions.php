<?php

namespace Drupal\media_entity_download;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media\Entity\MediaType;

/**
 * Provides dynamic permissions for downloading media of different types.
 */
class DownloadPermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of download permissions.
   *
   * @return array
   *   The media type permissions.
   *
   * @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function mediaTypePermissions() {
    $perms = [];

    // Generate download related permissions for all media types.
    /** @var MediaType $type */
    foreach (MediaType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    return $perms;
  }

  /**
   * Returns a list of download permissions for a given media type.
   *
   * @param MediaType $type
   *   The node type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(MediaType $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "download published $type_id media" => [
        'title' => $this->t('%type_name: Download published', $type_params),
      ],
      "download own unpublished $type_id media" => [
        'title' => $this->t('%type_name: Download own unpublished', $type_params),
      ],
      "download any unpublished $type_id media" => [
        'title' => $this->t('%type_name: Download any unpublished', $type_params),
      ],
    ];
  }

}
