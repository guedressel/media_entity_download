<?php

namespace Drupal\Tests\media_entity_download\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\media\Entity\Media;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\user\Entity\Role;
use Symfony\Component\HttpFoundation\Response;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group media_entity_download
 */
class DownloadTest extends BrowserTestBase {

  use MediaTypeCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'system',
    'node',
    'media',
    'file',
    'media_entity_download',
  ];

  /**
   * Default user with permission to download published media entities.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $default_user;

  /**
   * Default testing media type.
   *
   * @var \Drupal\media\MediaTypeInterface
   */
  private $default_media_type;

  /**
   * Field definition of default testing media type source field.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  private $default_source_filed;



  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->default_user = $this->drupalCreateUser(['download any published media']);
    $this->drupalLogin($this->default_user);

    // Create bundle and modify form display.
    $this->default_media_type = $this->createMediaType('file', ['id' => 'testing']);
    $this->default_media_type->setThirdPartySetting('media_entity_download', 'enabled', 1);
    $this->default_media_type->save();
    $this->default_source_filed = $this->default_media_type->getSource()
      ->getSourceFieldDefinition($this->default_media_type);
  }


  /**
   * @param bool $published
   * @param \Drupal\Core\Session\AccountInterface $owner
   *
   * @return \Drupal\media\Entity\Media
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createMediaEntity($published, AccountInterface $owner) {
    $value = FileItem::generateSampleValue($this->default_source_filed);
    $media = Media::create([
      'name' => 'test',
      'bundle' => 'testing',
      $this->default_source_filed->getName() => $value['target_id'],

    ]);
    $media->setOwnerId($owner->id());
    if ($published) {
      $media->setPublished();
    }
    else {
      $media->setUnpublished();
    }
    $media->save();
    return $media;
  }


  /**
   * Shortcut to assert HTTP response status.
   *
   * @param int $expected Expected HTTP status code.
   * @param $message
   */
  protected function assertResponseStatus($expected, $message='') {
    $this->assertEquals($expected, $this->getSession()
      ->getStatusCode(), $message);
  }


  /**
   * Tests download of media file when download is enabled for the media type.
   */
  public function testDownloadEnabled() {
    // Create bundle and modify form display.
    $media_type = $this->createMediaType('file', ['id' => 'testDownloadEnabled']);
    $media_type->setThirdPartySetting('media_entity_download', 'enabled', 1);
    $this->assertSame(SAVED_UPDATED, $media_type->save());

    $fieldDefinition = $media_type->getSource()->getSourceFieldDefinition($media_type);
    $sourceField = $fieldDefinition->getName();
    $value = FileItem::generateSampleValue($fieldDefinition);
    $media = Media::create([
      'name' => 'test',
      'bundle' => 'testDownloadEnabled',
      $sourceField => $value['target_id'],
    ]);
    $media->setPublished();
    $media->save();

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponse(200);
  }

  /**
   * Tests download of media file when download is disabled for the media type.
   */
  public function testDownloadDisabled() {
    $media_type = $this->createMediaType('file', ['id' => 'testDownloadDisabled']);
    $fieldDefinition = $media_type->getSource()->getSourceFieldDefinition($media_type);
    $sourceField = $fieldDefinition->getName();
    $value = FileItem::generateSampleValue($fieldDefinition);
    $media = Media::create([
      'name' => 'test',
      'bundle' => 'testDownloadDisabled',
      $sourceField => $value['target_id'],
    ]);
    $media->setPublished();
    $media->save();

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponse(403);
    $this->assertResponseStatus(
      Response::HTTP_FORBIDDEN,
      "Anonymous user may download published media when 'download any published media' permission granted"
    );
  }


  /**
   * Tests download of published media entity files without being logged in.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testPublishedMediaDownloadByAnonymous() {
    $media_owner = $this->createUser();
    $media = $this->createMediaEntity(TRUE, $media_owner);
    $this->assertTrue($media->isPublished());

    $anonymous_role = Role::load(AccountInterface::ANONYMOUS_ROLE);
    $this->assertFalse(in_array('download any published media',
      $anonymous_role->getPermissions()));
    $this->assertFalse(in_array('download published testing media',
      $anonymous_role->getPermissions()));

    if ($this->loggedInUser) {
      $this->drupalLogout();
    }

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponseStatus(
      Response::HTTP_FORBIDDEN,
      "Anonymous user may not download published media without any download permission granted"
    );

    $anonymous_role->grantPermission('download any published media');
    $anonymous_role->save();

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponseStatus(
      Response::HTTP_OK,
      "Anonymous user may download published media when 'download any published media' permission granted"
    );

    $anonymous_role->revokePermission('download any published media');
    $anonymous_role->grantPermission('download published testing media');
    $anonymous_role->save();

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponseStatus(
      Response::HTTP_OK,
      "Anonymous user may download published media when 'download published %type media' permission granted"
    );
  }

  /**
   * Tests download of published media entity files by some logged in user (not
   * entity owner).
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testPublishedMediaDownloadByLoggedInUser() {
    $media_owner = $this->createUser();
    $media = $this->createMediaEntity(TRUE, $media_owner);

    $authenticated_role = Role::load(AccountInterface::AUTHENTICATED_ROLE);
    $this->assertFalse(in_array('download any published media',
      $authenticated_role->getPermissions()));
    $this->assertFalse(in_array('download published testing media',
      $authenticated_role->getPermissions()));

    $test_user = $this->createUser();
    $this->assertTrue($test_user->hasRole(AccountInterface::AUTHENTICATED_ROLE));
    $this->drupalLogin($test_user);

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponseStatus(
      Response::HTTP_FORBIDDEN,
      "Logged in user may not download published media without any download permission granted"
    );

    $authenticated_role->grantPermission('download any published media');
    $authenticated_role->save();

    $this->assertTrue($test_user->hasPermission('download any published media'));

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponseStatus(
      Response::HTTP_OK,
      "Logged in user may download published media when 'download any published media' permission granted"
    );

    $authenticated_role->revokePermission('download any published media');
    $authenticated_role->grantPermission('download published testing media');
    $authenticated_role->save();

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponseStatus(
      Response::HTTP_OK,
      "Logged in user may download published media when 'download published %type media' permission granted"
    );
  }

  /**
   * Tests download of media entity files by it's owner.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testMediaDownloadByOwner() {
    $media_owner = $this->createUser(['download any published media']);
    $media_owner_role = Role::load(current($media_owner->getRoles(TRUE)));
    $no_media_owner = $this->createUser(['download any published media']);
    $no_media_owner_role = Role::load(current($no_media_owner->getRoles(TRUE)));
    $this->assertNotEqual($media_owner_role, $no_media_owner_role);
    $media = $this->createMediaEntity(TRUE, $media_owner);

    $this->drupalLogin($media_owner);

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponseStatus(
      Response::HTTP_OK,
      "Owner may download published media when 'download any published media' permission granted"
    );

    $media->setUnpublished();
    $media->save();

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponseStatus(
      Response::HTTP_FORBIDDEN,
      "Owner may not download unpublished media when 'download any/own unpublished %type media' permission not granted"
    );

    $media_owner_role->grantPermission('download any unpublished testing media');
    $media_owner_role->save();

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponseStatus(
      Response::HTTP_OK,
      "Owner may download unpublished media when 'download any unpublished %type media' permission granted"
    );

    $media_owner_role->revokePermission('download any unpublished testing media');
    $media_owner_role->grantPermission('download own unpublished testing media');
    $media_owner_role->save();

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponseStatus(
      Response::HTTP_OK,
      "Owner may download unpublished media when 'download own unpublished %type media' permission granted"
    );


    $this->drupalLogin($no_media_owner);

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponseStatus(
      Response::HTTP_FORBIDDEN,
      "Other user may not download unpublished media when 'download any unpublished %type media' permission not granted"
    );

    $no_media_owner_role->grantPermission('download any unpublished testing media');
    $no_media_owner_role->save();

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponseStatus(
      Response::HTTP_OK,
      "Other user may download unpublished media when 'download any unpublished  %type media' permission granted"
    );

    $no_media_owner_role->revokePermission('download any unpublished testing media');
    $no_media_owner_role->grantPermission('download own unpublished testing media');
    $no_media_owner_role->save();

    $this->drupalGet(Url::fromRoute('media_entity_download.download', ['media' => $media->id()]));
    $this->assertResponseStatus(
      Response::HTTP_FORBIDDEN,
      "Other user may not download unpublished media when 'download own unpublished %type media' permission granted"
    );

  }


  /**
   * Tests if force download option created correct HTTP response headers.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testForceDownload() {
    $media = $this->createMediaEntity(TRUE, $this->default_user);

    $this->drupalGet(Url::fromRoute('media_entity_download.download',
      ['media' => $media->id()],
      ['query' => ['force' => 'yes']]
    ));
    $this->assertResponseStatus(Response::HTTP_OK);
    $this->assertSession()->responseHeaderContains('Content-Disposition', 'attachment');

    $this->drupalGet(Url::fromRoute('media_entity_download.download',
      ['media' => $media->id()]
    ));
    $this->assertResponseStatus(Response::HTTP_OK);
    $this->assertSession()->responseHeaderNotContains('Content-Disposition', 'attachment');
  }
}
