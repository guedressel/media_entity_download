(function ($) {

  /**
   * Update the summary for the "download" vertical tab.
   */
  Drupal.behaviors.mediaEntityDownloadFieldsetSummary = {
    attach: function (context) {
      $('details#edit-media-entity-download', context).drupalSetSummary(function (context) {
        if ($('#edit-media-entity-download-enabled', context).prop('checked')) {
          return Drupal.t('Enabled');
        }
        else {
          return Drupal.t('Disabled');
        }
      });
    }
  };

})(jQuery);
